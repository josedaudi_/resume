# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.urls import reverse
from django.views.generic import TemplateView
from .models import *


class ResumeIndex(TemplateView):
    template_name = 'index.html'

    def get_context_data(self, **kwargs):
        context = super(ResumeIndex, self).get_context_data(**kwargs)
        context['resume_owner'] = ResumeOwner.objects.all()
        context['resume_owner_details'] = ResumeOwnerDetails.objects.all()[::1]
        context['social_accounts'] = ResumeOwnerSocialAccounts.objects.all()[1::]
        context['resume_owner_contacts'] = ResumeOwnerContacts.objects.all()[:]
        context['user'] = self.request.user
        print context
        # if context['resume_owner'] is None or context['resume_owner'] == '':
        #     return HttpResponseRedirect(reverse('install_resume'))
        # else:
        return context
