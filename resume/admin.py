# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from .models import ResumeOwner, ResumeOwnerDetails, ResumeOwnerSocialAccounts, ResumeOwnerContacts


class ResumeOwnerAdmin(admin.ModelAdmin):
    list_display = ('first_name', 'middle_name', 'last_name', 'designation')


class ResumeOwnerDetailsAdmin(admin.ModelAdmin):
    list_display = ('owner', 'date_of_birth', 'gender', 'nationality')


class ResumeOwnerSocialAccountsAdmin(admin.ModelAdmin):
    list_display = ('owner', 'linkedin_url', 'github_url', 'stackoverflow_url')


class ResumeOwnerContactsAdmin(admin.ModelAdmin):
    list_display = ('owner', 'email', 'mobile', 'fax')


admin.site.register(ResumeOwner, ResumeOwnerAdmin)
admin.site.register(ResumeOwnerDetails, ResumeOwnerDetailsAdmin)
admin.site.register(ResumeOwnerSocialAccounts, ResumeOwnerSocialAccountsAdmin)
admin.site.register(ResumeOwnerContacts, ResumeOwnerContactsAdmin)
