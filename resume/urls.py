from django.conf.urls import url
from .views import ResumeIndex

urlpatterns = [
    url(r'^$', ResumeIndex.as_view(), name='resume_index')
]